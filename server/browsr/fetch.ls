
show = -> console.log it
query = -> document.querySelector it
all = -> document.querySelectorAll it

socket = new WebSocket \ws://localhost:8000
socket.onopen = ->
  all-table = []
  tables = all \table
  for table in tables
    trs = table.querySelectorAll \tr
    all-list = []
    for tr in trs
      each-list = []
      for td in tr.children
        each-list.push td.innerText
      all-list.push each-list
    all-table.push all-list
  json = JSON.stringify all-table
  socket.send json
  show \open