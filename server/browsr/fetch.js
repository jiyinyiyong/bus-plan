var show, query, all, socket;
show = function(it){
  return console.log(it);
};
query = function(it){
  return document.querySelector(it);
};
all = function(it){
  return document.querySelectorAll(it);
};
socket = new WebSocket('ws://localhost:8000');
socket.onopen = function(){
  var allTable, tables, i$, len$, table, trs, allList, j$, len1$, tr, eachList, k$, ref$, len2$, td, json;
  allTable = [];
  tables = all('table');
  for (i$ = 0, len$ = tables.length; i$ < len$; ++i$) {
    table = tables[i$];
    trs = table.querySelectorAll('tr');
    allList = [];
    for (j$ = 0, len1$ = trs.length; j$ < len1$; ++j$) {
      tr = trs[j$];
      eachList = [];
      for (k$ = 0, len2$ = (ref$ = tr.children).length; k$ < len2$; ++k$) {
        td = ref$[k$];
        eachList.push(td.innerText);
      }
      allList.push(eachList);
    }
    allTable.push(allList);
  }
  json = JSON.stringify(allTable);
  socket.send(json);
  return show('open');
};