
require! \ws
require! \fs
show = console.log

wss = new ws.Server host: \0.0.0.0 port: 8000
wss.on \connection (socket) ->
  show \message
  socket.on \message (message) ->
    fs.write-file \server/a.json message, \utf8